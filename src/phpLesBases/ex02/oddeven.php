<?php

// boucle infini
echo 'Entrez un nombre: ';
while (true) {
    // affichage du input

    $pile = trim(fgets(STDIN));

    // controle si le nombre est un numerique
    if (is_numeric($pile)) {
        // controle si le nombre est pair ou impaire
        if ($pile % 2 == 0) {
            echo 'Le chiffre ' . $pile . " est Pair\nEntrez un nombre: ";
        } else {
            echo 'Le chiffre ' . $pile . " est Impair\nEntrez un nombre: ";
        }
    } else {
        echo "'" . $pile . "' n'est pas un chiffre\nEntrez un nombre: ";
    }
}
