<?php

// creation du tableau final vide
$final = [];

// recuperation de la liste des arguments sans le premier

if ($argc <= 1) {
    return;
}

// recuperation de la liste des arguments sans le premier
$chaine = array_slice($argv, 1);

    // recuperation du nombre d'arguments sans le premier
$NumArg = count($chaine);

    // var_dump($chaine);
for ($i = 1; $i < $NumArg; ++$i) {
    // split la chaine par rapport au ":"
    $tab = preg_split('/:/', $chaine[$i], -1, PREG_SPLIT_NO_EMPTY);

    // alimentation du tableau final
    if (count($tab) == 2) {
        $final[$tab[0]] = $tab[1];
    }
}

// resultat

if (array_key_exists($argv[1], $final)) {
    echo $final[$argv[1]] . "\n";
}
