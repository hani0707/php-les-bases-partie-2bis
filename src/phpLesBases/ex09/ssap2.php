<?php

// creation d'une liste vide
$listtotal = [];

// recuperation de la liste des arguments sans le premier
$chaine = array_slice($argv, 1);

// controle de la liste des arguments en splittant ceux qui auraient des sous arguments et je mets les elements
// dans la listetotal

foreach ($chaine as $el) {
    if (preg_split("/\s+/", $el, -1, PREG_SPLIT_NO_EMPTY) != false) {
        $listmot = preg_split("/\s+/", $el, -1, PREG_SPLIT_NO_EMPTY);

        foreach ($listmot as $al) {
            array_push($listtotal, $al);
        }
    }
}

// creation des listes vides pour mettres les numeriques , les letters et les autres
$listLettre = [];
$listNum = [];
$listAutre = [];

// triage des elements dans les listes concernées c'est à dire les chiffres dans la liste listNum , les lettres dans listLettre et
// les autres dans listAutre

foreach ($listtotal as $el) {
    if (preg_match('/[A-Za-z]/', $el[0]) == 1) {
        array_push($listLettre, $el);
    } elseif (preg_match('/[0-9]/', $el[0]) == 1) {
        array_push($listNum, $el);
    } else {
        array_push($listAutre, $el);
    }
}

// triage des listes

natcasesort($listLettre);

sort($listNum, SORT_STRING);

sort($listAutre);

// affichage des listes selon les criteres demandés

foreach ($listLettre as $el) {
    echo $el . "\n";
}

foreach ($listNum as $el) {
    echo $el . "\n";
}

foreach ($listAutre as $el) {
    echo $el . "\n";
}
