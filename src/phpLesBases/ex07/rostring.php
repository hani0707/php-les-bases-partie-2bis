<?php

if ($argc > 1) {
    // je l'enleve le nom du fichier de mon tableau
    array_slice($argv, 1);

    // recuperation du premier element de mon tableau
    $chaine = $argv[1];

    $tab = preg_split("/[^\S\r\n]/", $chaine, -1, PREG_SPLIT_NO_EMPTY);
    $debut = $tab[0];
    $tab = array_slice($tab, 1);
    array_push($tab, $debut);
    echo implode(' ', $tab) . "\n";
}
