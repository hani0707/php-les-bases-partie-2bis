<?php

$text = file_get_contents($argv[1]);

preg_match_all('/<a.*>.*<\/a>/', $text, $matchRef);

if (isset($matchRef[0])) {
    foreach ($matchRef[0] as $el) {
        $ligne = $el;
        $savEl = $el;
        preg_match_all('/>.*</', $ligne, $mot);
        $final = preg_replace('/<.*>/', '', $mot[0]);
        $gras = strtoupper($final[0]);
        $text = str_replace($final[0], $gras, $text);
    }
}

preg_match_all('/title=".*</', $text, $match);

foreach ($match[0] as $el) {
    $mot = str_replace('title', '', $el);
    $mot = strtoupper($mot);
    $mot = 'title' . $mot;
    $text = str_replace($el, $mot, $text);
}

echo $text;
