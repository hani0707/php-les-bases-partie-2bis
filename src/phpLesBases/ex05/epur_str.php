<?php

$chaine = array_splice($argv, 1);
if (count($chaine) == 1) {
    $tab = preg_split("/[^\S\r\n]/", $chaine[0], -1, PREG_SPLIT_NO_EMPTY);
    echo trim(implode(' ', $tab)) . "\n";
}
