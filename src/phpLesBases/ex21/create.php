<?php

$repertoire = './private';
$file = './private/passwd';
$listUsers = [];
$login = $_POST['login'];
$mdp = $_POST['passwd'];

function putUser(
        $logUser,
        $passUser,
        $fichier,
        $users)
{
    if (!$logUser == null && !$passUser == null) {
        $InfoConn = ['login' => $logUser,
             'passwd' => password_hash($passUser, PASSWORD_DEFAULT), ];
        $users[] = $InfoConn;
        file_put_contents($fichier, serialize($users));
        echo "OK\n";
        exit;
    } else {
        echo "ERROR\n";
        exit;
    }
}

        if (!file_exists($file)) {
            mkdir($repertoire, 0777, false);
            putUser($login, $mdp, $file, []);
        }

if ($_POST['submit'] == null) {
    echo "ERROR\n";
    exit;
}

if (file_get_contents($file)) {
    $users = file_get_contents($file);
    $listUsers = unserialize($users);
    foreach ($listUsers as $el) {
        if ($el['login'] == $_POST['login']) {
            echo "ERROR\n";

            return;
        }
    }

    putUser($login, $mdp, $file, $listUsers);
}
